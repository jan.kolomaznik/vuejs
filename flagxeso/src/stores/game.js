import { ref, reactive, computed } from 'vue'
import countriesJson from "@/assets/countries.json"
import { defineStore } from 'pinia'

export const useGameStore = defineStore('game', {
    state: () => ({
        players: null,
        playerOnTurn: null,
        countries: countriesJson.map(
            c => ({
              code: c.code,
              name: c.name,
              flag: c.flags.svg,
              borders: c.borders,
              status: 'hide'
            })
          ),
    }),
    getters: {
        country: (state) => {
            return code => state.countries.find(c => c.code == code)
        },
        countryWithNeighbour: (state) => {
          const openCountries = state.countries.filter(c => c.status == 'hide' || c.status == 'show')
          const neighbours = new Set(openCountries.map(c => c.borders).flat())
          return openCountries.filter(c => neighbours.has(c.code)).map(c => c.code)
        }
    },
    actions: {
        onStart(playerLeftName, playerRightName) {
            this.players = {
                left: {
                    name: playerLeftName,
                    countries: [],
                },
                right: {
                    name: playerRightName,
                    countries: [],
                }
            }
            this.playerOnTurn = 'left'
        },

        hideAllShowFlags() {
            this.countries.forEach(country => {
                if (country.status == 'show') {
                    country.status = 'hide'
                }
            });
        },

        showFlag(code) {
            this.countries.forEach(country => {
                if (country.code == code) {
                    country.status = 'show'
                }
            });
        },

        setOwener(player, ...codes) {
            this.countries.forEach(country => {
                if (codes.includes(country.code)) {
                    country.status = player
                    this.players[player].countries.push(country)
                }
            });
            const countryWithNeighbour = this.countryWithNeighbour
            this.countries.filter(c => !countryWithNeighbour.includes(c.code))
                          .forEach(c => c.status = 'close')

        },

        nextPlayer() {
            if (this.playerOnTurn == 'left') {
                this.playerOnTurn = 'right'
            } else {
                this.playerOnTurn = 'left'
            }
        }
    },
})
    


  /*
import { defineStore } from "pinia";

export const useCategoryStore = defineStore({
  id: "category",
  state: () => ({
    lang: null,
    categories: [],
    loading: false,
    error: null,
  }),
  getters: {
    getLang: (state) => state.lang,
    findById: (state) => {
      return (categoryId) => {
        console.log("findById state:", state);
        return state.categories.find((category) => category.id === categoryId);
      };
    },
  },

  actions: {
    async fetchCategories(lang) {
      this.lang = lang;
      this.categories = null;
      this.loading = true;
      this.error = null;
      try {
        const response = await fetch(`/api/${lang}/index.json`);
        this.categories = await response.json();
      } catch (error) {
        this.error = error;
        console.log(error);
      } finally {
        this.loading = false;
        this.lang = lang;
      }
    },
  },
});

*/
