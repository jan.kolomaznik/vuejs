import { unref } from "vue"

export function validate(i, s) {
    i = unref(i)
    s = unref(s)

    if (i == s) {
      return `Uhadl jsi tajne cislo ${s}`
    }
    if (i < s) {
      return `Tvuj odhad ${i} je prilis maly`
    }
    if (i > s) {
      return `Tvuj odhad ${i} je prilis velky`
    }
  }